# Dev Starter Pack

Based on Google Web Starter Kit

## Requirements
- Nodejs
- Git and Git Bash (for Windows)
- Yarn Package Manager or NPM
- Ruby (for Sass)
- Python

## Installation
1. Download as zip
2. Extract files
3. Go to extraction folder
4. Run `npm run setup`

## Commands
- `gulp serve` - run a live reload server
- `gulp` - generate distribution folder
- `gulp serve:dist` - run a live reload server based on distribution folder files

## Instructions

### HTML
- All working files should be inside the `app` folder
- Processes: Minification (disabled by default)

### CSS & SASS
- All SASS partials used for `@import` should be inside the `_sass_partials/` folder.
- Main stylesheet file and other stylesheets should be inside the `styles/` folder. Each SASS or CSS under this folder file will be processed separately and *will not* be concatenated into one file.
- Processes: Autoprefix, Group CSS Media Queries, Minify

### Javascript
- Prebody scripts (scripts called before the body tag) should be inside `scripts/prebody` folder. All scripts here will be concatenated into *one* file: `prebody.js` and minified as `prebody.min.js`
- Main script file and plugin files should be inside the `scripts/main/` folder. All files inside this folder will be concatenated into *one* file: `scripts/main.js` and minified as `scripts/main.min.js`
- All files directly under `scripts/` will be minified and copied to distribution folder individually (no concatenation).
- Processes: Concatenation, Minification

### Images
- Images should be inside `images/` folder. JPG, PNG, GIF, and SVG will be optimized automatically upon generating the distribution folder.

### Other Folders
- Folders not mentioned above will skip automation and will be copied directly upon generating the distribution folder.
