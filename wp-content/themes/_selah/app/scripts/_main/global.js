/*! Custom scripts */

function resizeVal(inp,reduce) {
  var inp = parseFloat(inp),
      factor = 0.5;
  if (reduce) {
    inp -= factor;
  } else {
    inp += factor;
  }
  //console.log(inp);
  return inp;
}

function setSelectedIndex(s, valsearch) {
  for (i = 0; i < s.options.length; i++) {

    if (s.options[i].value == valsearch) {
      s.options[i].selected = true;
      break;
    }
  }
  return;
}

function transposeChord(chord, amount) {
  var scale = ['C','C#','D','D#','E','F','F#','G','G#','A','Bb','B'],
      reb = /[CDEFGAB][b]/g,
      re = /[CDEFGAB][#]?/g;

  if (chord.match(reb)) {
    scale = ['C','Db','D','Eb','E','F','Gb','G','Ab','A','Bb','B'];
    re = reb;
  }
  //insert padding script here
  return chord.replace(re,
    function(match) {
      var i = (scale.indexOf(match) + amount) % scale.length;
      return scale[i < 0 ? i + scale.length : i];
    });
}

function computeDistance(source, target) {
  var scale = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B'],
      reb = /[CDEFGAB][b]/g,
      sourcePos,
      targetPos;

  if (source.match(reb)) {
    scale = ['C','Db','D','Eb','E','F','Gb','G','Ab','A','Bb','B'];
  }

  sourcePos = scale.indexOf(source);
  targetPos = scale.indexOf(target);

  return (targetPos-sourcePos);
}
var entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
};

function escapeHtml(string) {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  });
}
function ajax_download(url, data) {
    var $iframe,
        iframe_doc,
        iframe_html;

    if (($iframe = $('#download_iframe')).length === 0) {
        $iframe = $("<iframe id='download_iframe'" +
                    " style='display: none' src='about:blank'></iframe>"
                   ).appendTo("body");
    }

    iframe_doc = $iframe[0].contentWindow || $iframe[0].contentDocument;
    if (iframe_doc.document) {
        iframe_doc = iframe_doc.document;
    }

    iframe_html = "<html><head></head><body><form method='POST' action='" +
                  url +"'>"

    Object.keys(data).forEach(function(key){
        iframe_html += "<input type='hidden' name='"+key+"' value='"+data[key]+"'>";

    });

        iframe_html +="</form></body></html>";

    iframe_doc.open();
    iframe_doc.write(iframe_html);
    $(iframe_doc).find('form').submit();
}

function labelSongBlocks() {

  $('.song-block').each(function(){
    var $this = $(this),
        strTitle = $this.find('.block-title strong').text().replace(/ /g,'-'),
        title = strTitle.split(':');
    console.log(title);
    $this.addClass('block-'+title[0].toLowerCase());
  });
}

function attachNotesBox() {
  $('.song-block').not('.block-key').append('<div class="notes-block"/>');
  $('.song-block .lyrics-visible').closest('.song-block').addClass('block-lyrics-visible');
}

function removeNotesBox() {
  $('.notes-block').remove();
}

$(document).ready(function () {
  console.log('code26.ok');

  $(window).on('resize', _.debounce(function () {
    console.log('resized!');
  }, 300));

  $('.mode-toggle').on('click',function(){
    var $this = $(this),
        $parent = $this.closest('.song-section'),
        $sheet = $('.sheet-content',$parent),
        origText = $this.html(),
        text1 = 'Show Chords',
        text2 = 'Lyrics Only';
    if ($sheet.hasClass('lyrics-only')) {
      $this.html(text2);
    } else {
      $this.html(text1);
    }

    $sheet.toggleClass('lyrics-only');
    $parent.toggleClass('lyrics-only');
  });

  // button event for printing with notes box
  $('.btn-print').on('click',function(e){
    e.preventDefault();
    window.print();
  });

  // button event for printing with notes box.
  $('.btn-print-with-notes').on('click',function(e){
    e.preventDefault();

    $('body').addClass('with-notes');
    window.print();

  });

  $('.btn-increase').on('click',function(e){
    e.preventDefault();

    var $this = $(this),
        $parent = $this.closest('.song-section'),
        $sheet = $('.sheet-content',$parent),
        fontSize = $sheet.css('font-size');
    $sheet.css('font-size',resizeVal(fontSize));

  });

  $('.btn-decrease').on('click',function(e){
    e.preventDefault();

    var $this = $(this),
        $parent = $this.closest('.song-section'),
        $sheet = $('.sheet-content',$parent),
        fontSize = $sheet.css('font-size');

    $sheet.css('font-size',resizeVal(fontSize,1));
  });

  $('.btn-pdf').on('click',function(e){
    e.preventDefault();
    var $sheet = $('#page-container').html(),
        $head = document.getElementsByTagName('head')[0].innerHTML,
        $title = document.title;
    //generatePDF($source, $title);
    //$.download(tplUrl+'/generatePDF.php','fileName=' + $title + '&packet=' + $source);

    $head = $head.replace(/'/g, '"');
    $sheet = escapeHtml($sheet);
    ajax_download(tplUrl+'/generatePDF.php', {'fileName': $title, 'head' : $head, 'sheet': $sheet});
  });

  //pre-select Key on Transpose component
  var $transposer = $('.transposer'),
      $songSection = $('.song-section');

  if ($transposer.length) {
    $transposer.on('change',function(){
      var $this = $(this),
          $parent = $this.closest('.song-section'),
          $chord = $('.chord',$parent),
          selectVal = $this.val(),
          key = $('.chord.key',$parent).html().replace(/m/g, ''),
          distance = computeDistance(key,selectVal);
      console.log(key,selectVal,distance);

      $chord.each(function(){
        var $this = $(this),
            currentValue = $this.html();
        $this.html(transposeChord(currentValue,distance));
      });
    });
  }

  $songSection.each(function(idx){
    var $this = $(this),
        $transposer = $this.find('.transposer'),
        $presetKey = $this.attr('data-songkey'),
        key = $('.chord.key',$this).html().replace(/m/g, '');

        setSelectedIndex($transposer[0],key);

        if ($presetKey) {
          $transposer.val($presetKey).trigger('change');
        }
  });


  $('#SEARCHSONG').on('submit',function(e){
    e.preventDefault();

    var $this = $(this),
        values = $this.serialize();

    ajaxUpdateList(values);
  });

  //filters
  function ajaxUpdateList(packet) {
    xhr = $.ajax({
      url: tplUrl+'/q.php',
      dataType:"text",
      cache: true,
      contentType: "application/x-www-form-urlencoded;charset=utf-8",
      data: {
        'type' : 'update-list',
        'formdata' : packet
      },
      success: function(data) {
        //console.log('success');
        var $items = $('.song-items');
        $items.html(data);
      },
      error: function(data) {
        //console.log('error');
        $items.html('Sorry. We\'ve encountered an error processing the request.') ;
      }
    });
  }

  function generatePDF(packet,title) {
    xhr = $.ajax({
      url: tplUrl+'/generatePDF.php',
      cache: true,
      method: 'post',
      data: {
        'fileName' : title,
        'packet' : packet
      },
      success: function(data) {
        console.log(data);
      },
      error: function(data) {
        //console.log('error');
        $items.html('Sorry. We\'ve encountered an error processing the request.') ;
      }
    });
  }

  /*var lines = $("pre").text().split("\n");
  $.each(lines, function(n, elem) {
      $('.sheet-content').append('<span class="line">'+elem+'</span>');
  });*/

  labelSongBlocks();
  attachNotesBox();

  window.onafterprint = function() {
    console.log('closed');
    $('body').removeClass('with-notes');
  }
});
