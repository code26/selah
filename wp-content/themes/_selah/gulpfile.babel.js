/**
 *
 *  Dev Starter Pack
 */

'use strict';

// This gulpfile makes use of new JavaScript features.
// Babel handles this without us having to do anything. It just works.
// You can read more about the new JavaScript features here:
// https://babeljs.io/docs/learn-es2015/

const app = {
    deploy: '../selah/', // deployment folder
    jpegQuality: 80,
    // no need to change these unless YKWYD
    app: 'app/',
    tmp: '.tmp/',
    styles: 'styles/',
    scripts: 'scripts/',
    images: 'images/',
    port: 41710,
    host: 'localhost',
    bower: 'bower_components/'
};

const fileName = {
    jsPrebody : 'init.js',
    jsPrebodyMin : 'init.min.js',
    jsMain: 'scripts.js',
    jsMainMin: 'scripts.min.js',
    cssMain: 'style.css',
    cssMainMin: 'style.min.css',
}

import path from 'path';
import gulp from 'gulp';
import del from 'del';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import gulpLoadPlugins from 'gulp-load-plugins';
import {output as pagespeed} from 'psi';
import pkg from './package.json';

const $ = gulpLoadPlugins({
            camelize: true,
            pattern: [
                'gulp-*',
                'gulp.*',
                'glob',
                'main-bower-files',
                'pageres',
                'imagemin-mozjpeg'
            ],
            replaceString: /\bgulp[\-.]/
          });

const reload = browserSync.reload;

var onError = function(err) {
    console.log(err);
};

var watchFiles = function() {
  gulp.watch([app.app + '**/*.html'], reload);
  gulp.watch([app.app + app.styles + '**/*.{scss,css}', app.app + '_sass_partials/**/*.{scss,css}'], ['styles', reload]);
  gulp.watch([app.app + app.scripts + '**/*.js'], ['scripts', 'scripts:head', 'scripts:other', reload]);
  gulp.watch([app.app + app.images + '**/*.{jpg,png,gif,svg}'], ['images', reload]);
}

var watchDist = function() {
  //gulp.watch([app.app + '**/*.html'], reload);
  gulp.watch([app.tmp + app.styles + '**/*.css'], ['copy:styles']);
  gulp.watch([app.tmp + app.scripts + '**/*.js'], ['copy:scripts']);
  gulp.watch([app.tmp + app.images + '**/*'], ['copy:images']);
}

// Lint JavaScript
gulp.task('lint', () =>
  gulp.src([app.app + app.scripts +'**/*.js','!node_modules/**'])
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()))
);

gulp.task('cache:clear', function(done) {
    return $.cache.clearAll(done);
});

gulp.task('images', function(callback) {
  runSequence('images:optimize',callback);
});

// Optimize images
gulp.task('images:optimize', () =>
  gulp.src([
    app.app + app.images + '**/*.{jpg,png,svg,gif}'
  ])
    .pipe($.size({title: 'Original Images: '}))
    .pipe(
      $.imagemin([
        $.imagemin.gifsicle({
          interlaced: true
        }),
        $.imageminMozjpeg({
          quality: app.jpegQuality,
          progressive: true,
          //smooth: 10
        }),
        $.imagemin.optipng({
          optimizationLevel: 7
        }),
        $.imagemin.svgo({
          plugins: [{
            removeViewBox: true,
            removeEmptyAttrs: true,
            removeMetadata: true,
            removeUselessStrokeAndFill: true
          }]
        })
      ])
    )
    .pipe($.size({title: 'Optimized Images: '}))
    .pipe(gulp.dest(app.tmp + app.images))
);

// Copy all files at the root level (app)
gulp.task('copy', () =>
  gulp.src([
    app.app+'*/{,**}',
    '!'+app.app+'*/_*{,/**}',
    '!'+app.app+'_*/',
    '!'+app.app+'*.html',
    '!'+app.app+'*.scss',
    'node_modules/apache-server-configs/dist/.htaccess'
  ], {
    dot: true
  }).pipe(gulp.dest(app.tmp))
    .pipe($.size({title: 'copy'}))
);

gulp.task('copy:deploy', () =>
  gulp.src([
    app.tmp + '*',
    app.tmp + '*/**',
  ], {
    dot: true
  }).pipe(gulp.dest(app.deploy))
    .pipe($.size({title: 'copy:deploy'}))
);

gulp.task('copy:scripts', () =>
  gulp.src([
    app.tmp + app.scripts+'*',
    app.tmp + app.scripts+'*/**'
  ], {
    dot: true
  }).pipe(gulp.dest(app.deploy+app.scripts))
    .pipe($.size({title: 'copy:scripts'}))
);

gulp.task('copy:styles', () =>
  gulp.src([
    app.tmp + app.styles+'*',
    app.tmp + app.styles+'*/**',
  ], {
    dot: true
  }).pipe(gulp.dest(app.deploy+app.styles))
    .pipe($.size({title: 'copy:styles'}))
);

gulp.task('copy:images', () =>
  gulp.src([
    app.tmp + app.images+'*',
    app.tmp + app.images+'*/**',
  ], {
    dot: true
  }).pipe(gulp.dest(app.deploy+app.images))
    .pipe($.size({title: 'copy:images'}))
);

// Compile and automatically prefix stylesheets
gulp.task('styles', () => {
  const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
  ];

  // For best performance, don't add Sass partials to `gulp.src`
  return gulp.src([
    app.app + app.styles + '**/*.scss'
  ])
    .pipe($.plumber(onError))
    .pipe($.newer(app.tmp + app.styles))
    .pipe($.sass({
      precision: 10,
      //outputStyle: 'compressed',
      includePaths: [app.bower + 'bootstrap-sass/assets/stylesheets/', app.app + '_sass_partials/']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe($.groupCssMediaQueries({
        log: true
    }))
    .pipe($.concat(fileName.cssMain))
    .pipe($.sourcemaps.init())
    .pipe($.size({title: 'Styles: '}))
    .pipe(gulp.dest(app.tmp + app.styles))
    // Concatenate and minify styles
    .pipe($.if('*.css', $.cssnano()))
    .pipe($.size({title: 'Styles Minified: '}))
    .pipe($.rename(fileName.cssMainMin)) //
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(app.tmp + app.styles))
});

// Concatenate and minify JavaScript. Optionally transpiles ES2015 code to ES5.
// to enable ES2015 support remove the line `"only": "gulpfile.babel.js",` in the
// `.babelrc` file.
gulp.task('scripts', () =>
    gulp.src($.mainBowerFiles({
        filter: ['**/*.js', '!**/'+app.bower+'jquery/**/*']
      })
      .concat([
        app.app + app.scripts + '_main/*.js'
      ]))
      .pipe($.plumber(onError))
      .pipe($.newer(app.tmp + app.scripts + fileName.jsMain))
      .pipe($.babel())
      .pipe($.concat(fileName.jsMain))
      .pipe($.sourcemaps.init())
      .pipe(gulp.dest(app.tmp + app.scripts))
      .pipe($.size({title: 'Scripts: '}))
      .pipe($.uglify({preserveComments: 'some'}))
      // Output files
      .pipe($.rename(fileName.jsMainMin)) //
      .pipe($.size({title: 'Scripts Minified: '}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest(app.tmp + app.scripts))
);

//process jquery from bower
gulp.task('scripts:jquery', () =>
    gulp.src($.mainBowerFiles({
            filter: ['**/'+app.bower+'jquery/**/*']
        }))
      .pipe($.plumber(onError))
      .pipe($.newer('.tmp/scripts/lib/jquery.*'))
      .pipe($.babel())
      .pipe($.sourcemaps.init())
      .pipe($.concat('jquery.min.js'))
      .pipe($.uglify({preserveComments: 'some'}))
      // Output files
      .pipe($.size({title: 'Scripts jQuery: '}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest(app.tmp + app.scripts + 'lib/'))
);

gulp.task('scripts:other', () => {
  return gulp.src([app.app + app.scripts + '*.js' ])
    .pipe($.uglify({preserveComments: 'some'}))
    .pipe($.size({title: 'Other Scripts: '}))
    .pipe(gulp.dest(app.tmp + app.scripts))
});

//process scripts in head folder
gulp.task('scripts:head', () =>
    gulp.src([app.app + app.scripts + '_head/*.js'])
      .pipe($.plumber(onError))
      .pipe($.babel())
      .pipe($.concat(fileName.jsPrebody))
      .pipe($.newer(app.tmp + app.scripts + fileName.jsPrebody))
      .pipe($.sourcemaps.init())
      .pipe($.size({title: 'Scripts Prebody: '}))
      .pipe(gulp.dest(app.tmp + app.scripts))
      .pipe($.uglify({preserveComments: 'some'}))
      // Output files
      .pipe($.rename(fileName.jsPrebodyMin)) //
      .pipe($.size({title: 'Scripts Prebody Minified: '}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest(app.tmp + app.scripts))
);

// HTML
gulp.task('html', () => {
  return gulp.src('app/**/*.html')
    .pipe($.useref({
      searchPath: '{.tmp,app}',
      noAssets: true
    }))

    // Minify any HTML
    // .pipe($.if('*.html', $.htmlmin({
    //   removeComments: true,
    //   collapseWhitespace: true,
    //   collapseBooleanAttributes: true,
    //   removeAttributeQuotes: true,
    //   removeRedundantAttributes: true,
    //   removeEmptyAttributes: true,
    //   removeScriptTypeAttributes: true,
    //   removeStyleLinkTypeAttributes: true,
    //   removeOptionalTags: true
    // })))
    // Output files
    .pipe($.if('*.html', $.size({title: 'Html: ', showFiles: true})))
    .pipe(gulp.dest(app.tmp));
});

// Clean output directory
gulp.task('clean', () => {
  console.log('\x1b[41m', 'CLEARING .TMP FOLDER!','\x1b[0m')
  del(['.tmp', app.tmp +'*', '!'+app.tmp+'.git'], {dot: true})
});

// run BrowserSync and Watch functions
gulp.task('live', () => {
  browserSync({
    notify: false,
    // Customize the Browsersync console logging prefix
    logPrefix: 'DSP',
    // Allow scroll syncing across breakpoints
    //scrollElementMapping: ['main', '.mdl-layout'],
    // Run as an https by uncommenting 'https: true'
    // Note: this uses an unsigned certificate which on first access
    //       will present a certificate warning in the browser.
    // https: true,
    server: ['.tmp','app'],
    tunnel: true,
    port: app.port
  });

  watchFiles();
});

// Watch files for changes & reload
gulp.task('serve', ['clean'], cb =>
  runSequence(
    'styles',
    ['scripts:jquery','scripts:other', 'scripts:head', 'scripts', 'images'],
    'live',
    cb
  )
);

// Build and serve the output from the dist build
gulp.task('serve:deploy', ['deploy'], () => {
  watchFiles();
  watchDist();
});

// Build production files, the default task
gulp.task('default', ['clean'], cb =>
  runSequence(
    'styles',
    ['html','scripts:jquery','scripts:other', 'scripts:head', 'scripts', 'images'],
    'copy',
    cb
  )
);

// Build production files, the default task
gulp.task('deploy', cb =>
  runSequence(
    'default',
    'copy:deploy',
    cb
  )
);

// Run PageSpeed Insights
gulp.task('pagespeed', cb =>
  // Update the below URL to the public URL of your site
  pagespeed('example.com', {
    strategy: 'mobile'
    // By default we use the PageSpeed Insights free (no API key) tier.
    // Use a Google Developer API key if you have one: http://goo.gl/RkN0vE
    // key: 'YOUR_API_KEY'
  }, cb)
);
