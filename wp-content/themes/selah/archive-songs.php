<?php get_header(); ?>

  <div class="page-container">
    <section class="page-section search-container">
      <form action="" id="SEARCHSONG">
        <div class="form-group form-group-lg">
          <div class="input-group">
            <input type="text" class="form-control" name="keyword" id="keyword" placeholder="title, artist, lyrics, tags">
            <div class="input-group-addon">
              <button type="submit" class="btn btn-primary btn-search">Search</button>
            </div>
          </div>
        </div>
      </form>
    </section>
    <section class="page-section song-list">
      <ul class="song-items">
      <?php
      global $query_string;
      query_posts( $query_string . '&posts_per_page=-1&orderby=title&order=ASC' );
      while ( have_posts() ) : the_post();
      ?>

      <?php include 'partials/list-item-song.php'; ?>

      <?php endwhile; ?>
      </ul>
    </section>
  </div>

<?php get_footer();?>
