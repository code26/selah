
<script>window.jQuery || document.write('<script src="<?php bloginfo( 'template_url' ); ?>/scripts/lib/jquery.min.js"><\/script>')</script>
<script src="<?php bloginfo( 'template_url' ); ?>/scripts/scripts.min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-26818629-2', 'auto');
  ga('require', 'autotrack');
  ga('send', 'pageview');

</script>
<?php wp_footer();?>
<!--
<?php echo get_num_queries(); ?> queries in <?php timer_stop(1); ?> seconds

<?php
sleep(1);
$time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo "Process Time: {$time}";
?>

-->
</body>
</html>
