<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <title><?php
  /*
   * Print the <title> tag based on what is being viewed.
   */
  global $page, $paged;

  wp_title( '|', true, 'right' );

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', 'selah' ), max( $paged, $page ) );

  ?></title>

  <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/styles/style.min.css">

  <script src="<?php bloginfo( 'template_url' ); ?>/scripts/init.min.js"></script>

  <script>
    var tplUrl = '<?php bloginfo( 'template_url' ); ?>';
  </script>

  <?php wp_head();?>

</head>

<body <?php body_class(); ?>>
