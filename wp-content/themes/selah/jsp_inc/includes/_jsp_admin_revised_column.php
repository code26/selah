<?php

add_filter('manage_pages_columns', 'add_revised_column');
add_action('manage_pages_custom_column', 'echo_revised_column', 10, 2);

function add_revised_column($columns) {

    $columns['revised'] = 'Revised';

    return $columns;
}


function echo_revised_column($column, $id) {

    if ('revised' == $column)
        echo get_post_field('post_modified', $id);
}
