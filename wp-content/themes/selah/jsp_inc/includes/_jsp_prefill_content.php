<?php

add_filter( 'default_content', 'my_editor_content' );

function my_editor_content( $content ) {
    global $post_type;
    if( 'songs' == $post_type )
    $content = '<strong>Key: A</strong>


<strong>Intro:</strong>
F#m E D E (2x), Bm F#m D E (2x)


<strong>Verse:</strong>
A         D      E
This is a sample content.

All Lyrics should be on Odd lines.

All Chords should be on Even lines.

You may refer to other existing entries for help.

<strong>Chorus:</strong>

Each song section (verse, chorus) should be wrapped in "strong" tags, like the one above.

Don\'t forget to select the proper Song Category or Categories of the song.

Have Fun!';

    return $content;
}

?>
