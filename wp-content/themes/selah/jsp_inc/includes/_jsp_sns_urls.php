<?php

$sns_url_setting = new sns_url_setting();

class sns_url_setting {
    function sns_url_setting( ) {

        add_filter( 'admin_init' , array( &$this , 'register_sns_fb_field' ) );
        add_filter( 'admin_init' , array( &$this , 'register_sns_tw_field' ) );
        add_filter( 'admin_init' , array( &$this , 'register_sns_ig_field' ) );
    }

    function register_sns_fb_field() {
        register_setting( 'general', 'sns_url_fb', 'esc_attr' );
        add_settings_field('sns_url_fb', '<label for="sns_url_fb">'.__('Facebook URL' , 'sns_url_fb' ).'</label>' , array(&$this, 'fields_fb_html') , 'general' );
    }
    function fields_fb_html() {
        $value = get_option( 'sns_url_fb', '' );
        echo '<input type="text" id="sns_url_fb" name="sns_url_fb" value="' . $value . '" />';
    }

    function register_sns_tw_field() {
        register_setting( 'general', 'sns_url_tw', 'esc_attr' );
        add_settings_field('sns_url_tw', '<label for="sns_url_tw">'.__('Twitter URL' , 'sns_url_tw' ).'</label>' , array(&$this, 'fields_tw_html') , 'general' );
    }
    function fields_tw_html() {
        $value = get_option( 'sns_url_tw', '' );
        echo '<input type="text" id="sns_url_tw" name="sns_url_tw" value="' . $value . '" />';
    }

    function register_sns_ig_field() {
        register_setting( 'general', 'sns_url_ig', 'esc_attr' );
        add_settings_field('sns_url_ig', '<label for="sns_url_ig">'.__('Instagram URL' , 'sns_url_ig' ).'</label>' , array(&$this, 'fields_ig_html') , 'general' );
    }
    function fields_ig_html() {
        $value = get_option( 'sns_url_ig', '' );
        echo '<input type="text" id="sns_url_ig" name="sns_url_ig" value="' . $value . '" />';
    }

}
