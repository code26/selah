<div class="tools hidden-print">
  <div class="form-group component-transpose">
    <label class="hidden-xs">Transpose:</label>
    <select class="form-control transposer">
      <option value="C">C</option>
      <option value="C#">C#</option>
      <option value="D">D</option>
      <option value="D#">D#</option>
      <option value="E">E</option>
      <option value="F">F</option>
      <option value="F#">F#</option>
      <option value="G">G</option>
      <option value="G#">G#</option>
      <option value="A">A</option>
      <option value="A#">A#</option>
      <option value="B">B</option>
    </select>
  </div>
  <button class="btn mode-toggle">Lyrics Only</button>
  <div class="text-resize">
    <button class="btn btn-decrease"><span>A-</span></button>
    <button class="btn btn-increase"><span>A+</span></button>
  </div>
  <button class="btn btn-print"><span class="glyphicon glyphicon-print"></span></button>
  <button class="btn btn-print-with-notes"><span class="glyphicon glyphicon-edit"></span></button>
  <!-- <button class="btn btn-pdf"><span class="glyphicon glyphicon-file"></span></button> -->
  <a href="/songs" class="btn btn-default  ">SONGS</a>
</div>
