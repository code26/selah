<?php
    $artist = rwmb_meta('jsp_song_artist');
    $songCats = get_the_terms($post->ID, 'song-category');
    $songTags = get_the_terms($post->ID, 'song-tags');
    $songCatIDs = $songCatNames = [];

    if (!empty($songCats)) {
      foreach($songCats as $songCat) {
        array_push($songCatIDs,$songCat->term_id);
        array_push($songCatNames,$songCat->slug);
      }
    }
  ?>
    <li class="song-item <?=implode(' ',$songCatNames);?>" data-catids="<?=implode(',',$songCatIDs);?>">
    <!-- <div class="cb-container">
      <input name="selected" value="<?=$post->ID?>" type="checkbox">
    </div> -->
    <div class="item-title">
      <a href="<?php the_permalink();?>"><?php the_title();?></a>
    </div>
    <div class="meta-data">
    <?php if (!empty($artist)): ?>
      <ul class="song-details">
        <li class="artist"><?=$artist?></li>
      </ul>
    <?php endif;?>
      <ul class="pub-details hidden-xs">
        <li class="modified"><?php the_modified_date('M j, Y'); ?></li>
      </ul>
    <?php if (!empty($songCats)): ?>
      <ul class="categories hidden-xs">
        <?php
          foreach($songCats as $songCat) {
        //print_r($songCat);
        ?>
          <li>
            <a href="#" data-catID="<?=$songCat->term_id;?>">
              <?=$songCat->name;?>
            </a>
          </li>
        <?php } ?>
      </ul>
    <?php endif;?>
    <?php if (!empty($songTags)): ?>
      <ul class="tags hidden-xs">
        <?php
          foreach($songTags as $songTag) {
        //print_r($songCat);
        ?>
          <li data-catID="<?=$songTag->term_id;?>">
            <a href="#">
              <?=$songTag->name;?>
            </a>
          </li>
        <?php }?>
      </ul>
    <?php endif; ?>
    </div>

  </li>
