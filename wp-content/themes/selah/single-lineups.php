<?php get_header(); ?>
<div id="page-container">
  <div class="page-container">
    <header class="lineup-header hidden-print">
      <div class="lineup-title">
        Lineup for: <?php the_title();?>
      </div>
    </header>

    <?php
      $lineup = rwmb_meta('jsp_lineup_song_item');
      if (!empty($lineup)):
        $songIDs = array();
        $songKeys = array();
        foreach($lineup as $song) {
          array_push($songIDs,$song["jsp_lineup_song"]);
          $songKeys[$song["jsp_lineup_song"]] = (object) [
            'key' =>  $song["jsp_lineup_song_key"],
          ];
        }
    ?>

    <?php
      $args = array(
        'post_type' => array( 'songs' ),
        'post__in'  => $songIDs,
        'orderby' => 'post__in',
        'order' => 'ASC',
      );
      $songs = get_posts( $args );
      foreach ( $songs as $post ) :
        setup_postdata( $post );
    ?>
    <section class="page-section song-section" data-songkey="<?=$songKeys[$post->ID]->key?>" data-songID="<?=$post->ID?>">
    <?php get_template_part('partials/component','songsheet')?>
    <div class="visible-print">
      <span class="printables">
        <span><?="Printed: ".date("M j Y")?></span>
        <span class="orig-key"><?="Original Key: ".getKey(get_the_content())?></span>
      </span>
    </div>
    </section>
    <?php endforeach; wp_reset_postdata();?>
    <?php endif; ?>

    <?php
      $resources = rwmb_meta('jsp_resource_list');

      if (!empty($resources)):
    ?>
    <div class="sheet-section resources lineup-resources hidden-print">
      <h3 class="section-title">Lineup Resources</h3>
      <ul>
        <?php foreach($resources as $resource):?>
        <li><?=$resource["jsp_song_resource_description"]?>: <?=make_clickable($resource["jsp_song_resource_url"])?></li>
        <?php endforeach;?>
      </ul>
    </div>
    <?php endif;?>
  </div>

</div>
<?php get_footer();?>
