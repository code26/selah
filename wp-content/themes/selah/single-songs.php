<?php get_header(); ?>
<div id="page-container">
  <div class="page-container">
    <section class="page-section song-section">
    <?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part('partials/component','songsheet')?>
    <?php endwhile; ?>
    </section>
  </div>
  <div class="visible-print">
    <span class="printables">
      <span><?="Printed: ".date("M j Y")?></span>
      <span class="orig-key"><?="Original Key: ".getKey(get_the_content())?></span>
    </span>
  </div>
</div>

<?php get_footer();?>
