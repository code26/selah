<?php get_header(); ?>

<div class="songs-container container-fluid">
  <div class="component-search-bar">
    <form action="" id="SEARCHSONG">
      <div class="input-group mb-2 mr-sm-2 mb-sm-0">
        <input type="text" class="form-control modern" id="SearchKeyword" name="keyword" placeholder="">
        <button type="submit" class="input-group-addon btn-search">SEARCH</button>
      </div>
    </form>
  </div>
  <div class="status-message">Showing all entries</div>
  <div class="songs-list">
    <div class="row items song-items">

      <?php
      global $query_string;
      query_posts( $query_string . '&posts_per_page=-1&orderby=title&order=ASC' );
      while ( have_posts() ) : the_post();
      ?>

      <?php include 'partials/list-item-song.php'; ?>

      <?php endwhile; ?>

      <?php get_template_part('partials/component','add-song');?>

    </div>
  </div>

  <!-- <a href="#" class="btn btn-primary btn-showall">Show All</a> -->

</div>
<?php get_sidebar('site-menu'); ?>

<?php get_footer();?>
