<?php
require __DIR__ . '/jsp_inc/vendor/autoload.php';

use mikehaertl\wkhtmlto\Pdf;

// Create a new Pdf object with some global PDF options
$sheet = isset($_POST['sheet']) ? $_POST['sheet'] : '';
$head = isset($_POST['head']) ? $_POST['head'] : '';
$fileName = isset($_POST['fileName']) ? $_POST['fileName'] : '';

$options = array(
  //'no-outline',           // option without argument
  'encoding' => 'UTF-8',
  'print-media-type',
);



$pdf = new Pdf(array(
  'page-size' => 'Letter',
));
$pdf->addPage('<html>' . $head . $sheet . '</html>',$options);
$pdf->send();
//$pdf->send($fileName . '.pdf');
var_dump($pdf);
