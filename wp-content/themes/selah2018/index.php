<?php get_header(); ?>

<div class="stage-container">
  <div class="items">
    <div class="mask-container">
      <img id="SelahMask" src="<?php bloginfo( 'template_url' ); ?>/media/selah-mask.svg" alt="">
    </div>
<?php if ( !is_user_logged_in() ) : ?>
    <div class="login-container">
        <a href="<?=wp_login_url();?>" class="btn btn-primary inblock btn-login-big">Login</a><br>
        <a href="<?=wp_registration_url();?>" class="link-inline register-link">Register</a>
    </div>
  </div>
<?php endif; ?>
</div>

<?php if ( is_user_logged_in() ) {
  get_sidebar('site-menu');
} ?>

<?php get_footer();?>
