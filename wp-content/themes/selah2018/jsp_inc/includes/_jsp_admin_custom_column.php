<?php

// ADD NEW COLUMN
function jsp_songs_columns_head($defaults) {
    $defaults['artist_name'] = 'Artist';
    return $defaults;
}

function jsp_songs_columns_content($column_name, $post_ID) {
    if ($column_name == 'artist_name') {
        $post_artist_name = rwmb_meta('jsp_song_artist','',$post_ID);
        if ($post_artist_name) {
            echo $post_artist_name;
        }
    }
}
function songs_sortable_columns( $columns ) {
    $columns['artist_name'] = 'artist_name';

    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);

    return $columns;
}
add_filter('manage_edit-songs_sortable_columns', 'songs_sortable_columns' );
add_filter('manage_songs_posts_columns', 'jsp_songs_columns_head', 10);
add_action('manage_songs_posts_custom_column', 'jsp_songs_columns_content', 10, 2);

function songs_column_order($columns) {
  $n_columns = array();
  $move = 'artist_name'; // what to move
  $before = 'author'; // move before this
  foreach($columns as $key => $value) {
    if ($key==$before){
      $n_columns[$move] = $move;
    }
      $n_columns[$key] = $value;
  }
  return $n_columns;
}
add_filter('manage_songs_posts_columns', 'songs_column_order', 10);

function jsp_artist_name_orderby( $query ) {
    if( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');

    if( 'artist_name' == $orderby ) {
        $query->set('meta_key','jsp_song_artist');
        $query->set('orderby','meta_value');
    }
}
add_action( 'pre_get_posts', 'jsp_artist_name_orderby' );
