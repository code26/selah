<?php

function maintenace_mode() {
  if ( !current_user_can( 'edit_themes' ) || !is_user_logged_in() ) {
    //wp_die('Maintenance.');
    $newURL =  get_site_url().'/index.html';
    header('Location: '.$newURL);
  }
}
add_action('get_header', 'maintenace_mode');
