<?php
function getKey($data) {
  //find and wrap Key
  $keyMatch = preg_match('/Key:\s?([ABCDEFG][#|b]?[m]?)/',$data,$key);
  return str_replace('Key: ', '',$key[0]);
}
function processSheet($data) {
  //remove extra white space from start/end
  $content = trim($data);

  //group sections
  $content = preg_replace('/(<strong>.+?<\/strong>[\w\s\d[\'’"ø,#\/\-:().!?]*]*)/','<div class="song-block">$0'."\n".'</div>',$content);

  //add class to block titles
  $content = preg_replace('/(<strong>.+?<\/strong>)/','<h3 class="block-title">$1</h3>',$content);

  //sections to show for lyrics mode
  $content = preg_replace('/<strong>((?:verse|chorus|pre-chorus|coda|refrain|koda|bridge|koro|talata|berso|tulay|pagtatapos|end|medley)+\s?[0-9]*?[:])<\/strong>/i', '<strong class="lyrics-visible">$1</strong>', $content);

  //find and wrap Key
  $content = preg_replace('/Key:\s?([ABCDEFG][#|b]?[m]?)/','Key: <a class="chord key">$1</a>',$content);

  //regexp chords
  //$content = preg_replace('/(?<![a-zA-Z0-9#])([BCDEFG]|[A](?!\h{1}[A-Za-z][aeiouy]))(#|b)?(maj|min|M|m|sus|7sus|9sus)?([0-9]{1,2})?(aug|dim|ø)?([0-9]{1,2})?(alt|#|b|M|Maj)?([0-9]{1,2})?([\)-\/]|[\),-]?\s{1}|$)/','<a class="chord">$1$2$3$4$5$6$7$8</a>$9',$content);

  $content = preg_replace('/(?<!I\s|a-zA-Z0-9#)([BCDEFG]|A(?!\h{1}[A-Za-z][aeiouy]))(#|b)?(maj|min|M|m|sus|7sus|9sus)?([0-9]{1,2})?(aug|dim|ø)?([0-9]{1,2})?(alt|#|b|M|Maj)?([0-9]{1,2})?([\)-\/]|[\),-]?\s{1}|$)/','<a class="chord">$1$2$3$4$5$6$7$8</a>$9',$content);

  //trim blank spaces before new line
  $content = preg_replace('/(\s+\n)/',"\n",$content);

  //wrap chords per line *Wraps everyting from first .chord until next line*
  $content = preg_replace('/([(-]*[\w: \/.]*<a class="chord".+?\/a>[ \w.()-]*\n+)/','<span class="chords">$0</span>',$content);

  //wrap lyrics per line
  $content = preg_replace('/(?<=<\/span>)(.+\n)/','<span class="lyrics">$0</span>',$content);

  $output = trim($content);

  return $output;
}
