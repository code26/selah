<?php
$hostname = gethostname();

function walled_garden() {
    if( ! is_home() && ! is_user_logged_in() )
        wp_redirect( '/wp-login.php' . '?redirect_to=' . esc_url($_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]) );
}
add_action( 'get_header', 'walled_garden' );

//No REST for guests
add_filter( 'rest_authentication_errors', function( $result ) {
    if ( ! empty( $result ) ) {
        return $result;
    }
    if ( ! is_user_logged_in() ) {
        return new WP_Error( 'Unauthorized Access', 'Sorry, you must be logged in to make a request.', array( 'status' => 401 ) );
    }
    return $result;
});
