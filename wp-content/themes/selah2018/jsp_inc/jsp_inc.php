<?php
//error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
//ini_set('display_errors', '1');

$includes_path = 'includes';

//composer auto load
//require __DIR__ . '/vendor/autoload.php';

// shortcodes
include get_template_directory() . '/jsp_inc/jsp_shortcodes.php';

// custom post types and taxonomies
include get_template_directory() . '/jsp_inc/jsp_posttypes.php';

add_filter('show_admin_bar', '__return_false');

function jsp_theme_setup()
{
    register_nav_menus(
        array(
            'primary' => __( 'Primary', 'jsp' ),
            'footer' => __( 'Footer', 'jsp' ),
            )
        );

}
add_action( 'after_setup_theme', 'jsp_theme_setup' );

add_post_type_support( 'page', 'excerpt' );

if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'square', 200, 200, true);
    add_image_size( 'squareM', 400, 400, true);
    add_image_size( 'cards', 267, 240);
}

function jsp_the_post_thumbnail($size = "default" , $type="jsp_featured") {
    global $post;

    if ( get_post_meta($post->ID, $type, true) ) {
        $thumbsize = $size;
        $images = get_post_meta( $post->ID, $type, false );
        $src = wp_get_attachment_image_src( $images[0], $thumbsize );
        echo $src[0]; //returns URL
    }
}

function jsp_the_background_image($size = "default" , $type="jsp_backgroundImg") {
    global $post;

    if ( get_post_meta($post->ID, $type, true) ) {
        $thumbsize = $size;
        $images = get_post_meta( $post->ID, $type, false );
        $src = wp_get_attachment_image_src( $images[0], $thumbsize );
        echo $src[0];
    }
}

function jsp_get_the_date($html5 = false)
{
    global $post;

    if ( get_post_meta( $post->ID, 'jsp_postdate', true) ) {
        $dateFormat = get_option('date_format');
        $postDate = get_post_meta( $post->ID, 'jsp_postdate', true);
        $formattedDate = date( $dateFormat, strtotime($postDate) );

        if ($html5) {
            $formattedDate = date_create($formattedDate);
            $formattedDate = date_format($formattedDate, 'Y-m-d H:i:s');
        }
        return $formattedDate;

    } else {
        if ($html5) {
            $formattedDate = date_create(get_the_date());
            $formattedDate = date_format($formattedDate, 'Y-m-d H:i:s');
            return $formattedDate;
        } else {
            return get_the_date();
        }
    }
}

function jsp_get_the_author()
{
    global $post;

    if ( get_post_meta( $post->ID, 'jsp_author', true) ) {
        $author = get_post_meta( $post->ID, 'jsp_author', true);

        return $author;

    } else {
        return get_the_author();
    }
}

function add_my_editor_style() {
    add_editor_style('style-editor.css');
}
add_action( 'admin_init', 'add_my_editor_style' );

function jsp_admin_styles()
{
    wp_register_style( 'jsp_admin_stylesheet', get_stylesheet_directory_uri(). '/style-admin.css' );
    wp_enqueue_style( 'jsp_admin_stylesheet' );
}
add_action( 'admin_enqueue_scripts', 'jsp_admin_styles' );

// get taxonomies terms links
function jsp_get_the_terms()
{
  // get post by post id
  $post = get_post( $post->ID );

  // get post type by post
  $post_type = $post->post_type;

  // get post type taxonomies
  $taxonomies = get_object_taxonomies( $post_type, 'objects' );

  $out = array();
  foreach ($taxonomies as $taxonomy_slug => $taxonomy) {

    // get the terms related to post
    $terms = get_the_terms( $post->ID, $taxonomy_slug );

    if ( !empty( $terms ) ) {
      foreach ($terms as $term) {
        $out[] =
          '  <a href="'
        .    get_term_link( $term->slug, $taxonomy_slug ) .'">'
        .    $term->name
        . "</a>";
      }
    }
  }
  return implode(',', $out );
}


function print_filters_for( $hook = '' ) {
    global $wp_filter;
    if( empty( $hook ) || !isset( $wp_filter[$hook] ) )
        return;

    print '<pre>';
    print_r( $wp_filter[$hook] );
    print '</pre>';
}


function twentyfourteen_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentyfourteen' ), max( $paged, $page ) );
    }

    return $title;
}
add_filter( 'wp_title', 'twentyfourteen_wp_title', 10, 2 );

function mb_body_classes( $classes ) {
    global $post;

    if ( is_front_page() || is_home() ) {
        $classes[] = 'homepage';
    } else {
        $classes[] = 'insidepage';
        $classes[] = 'common-page';
    }

    $slug = $post->post_name;
    $classes[] = $slug.'-page';

    $ptype = $post->post_type;
    $classes[] = $ptype.'-page';

    if ( is_archive() || is_search() || is_home() ) {
        $classes[] = 'list-view';
    }

    if ( is_archive() || is_search() || is_home() ) {
        $classes[] = 'list-view';
    }

    if ( is_singular() && ! is_front_page() ) {
        $classes[] = 'singular';
    }

    return $classes;
}
add_filter( 'body_class', 'mb_body_classes' );

/*// Re-define meta box path and URL
define( 'RWMB_URL', trailingslashit( get_stylesheet_directory_uri() . '/jsp_inc/meta-box' ) );
define( 'RWMB_DIR', trailingslashit( STYLESHEETPATH . '/jsp_inc/meta-box' ) );
// Include the meta box script
require_once RWMB_DIR . 'meta-box.php';*/
// Include the meta box definition (the file where you define meta boxes, see `demo/demo.php`)
include get_template_directory() . '/jsp_inc/metaboxes.php';

//Require taxonomy-meta
require_once get_template_directory() . '/jsp_inc/taxonomy-meta/taxonomy-meta.php';
//Include taxonomy-meta definitions
include get_template_directory() . '/jsp_inc/metasections.php';


if (class_exists('Nav_Menu_Images')) {

    function md_nmi_custom_content( $content, $item_id, $original_content ) {
      $content = $content . '<span class="link-title">' . $original_content . '</span>';

      return $content;
  }
  add_filter( 'nmi_menu_item_content', 'md_nmi_custom_content', 10, 3 );
}

function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

// Disables filters on textareas (e.g. category description)
foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
    remove_filter($filter, 'wp_filter_kses');
}

foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
    remove_filter($filter, 'wp_kses_data');
}

/**
 * Add to extended_valid_elements for TinyMCE
 *
 * @param $init assoc. array of TinyMCE options
 * @return $init the changed assoc. array
 */
function change_mce_options( $init ) {
 //code that adds additional attributes to the pre tag
 $ext = 'div[*],section[*],article[*],header[*],footer[*],nav[*]';

 //if extended_valid_elements alreay exists, add to it
 //otherwise, set the extended_valid_elements to $ext
 if ( isset( $init['extended_valid_elements'] ) ) {
  $init['extended_valid_elements'] .= ',' . $ext;
 } else {
  $init['extended_valid_elements'] = $ext;
 }

 //important: return $init!
 return $init;
}
add_filter('tiny_mce_before_init', 'change_mce_options');

function remove_empty_p($content){
    $content = force_balance_tags($content);
    return preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
}
add_filter('the_content', 'remove_empty_p', 20, 1);

function dashesToCamelCase($string, $capitalizeFirstCharacter = false) {
    $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));

    if (!$capitalizeFirstCharacter) {
        $str[0] = strtolower($str[0]);
    }

    return $str;
}

function jsp_switch_page_template() {
    global $post;

    if (is_page() && !get_page_template_slug( $post->ID )) { //if no template was selected via WP
        $ancestors = $post->ancestors;

        if ($ancestors) {
            $templatePath = TEMPLATEPATH . '/';
            $prefix = 'page-'; //default page template prefix
            $ext = '.php';
            $appender = '_child';
            $parent_id = get_post( end($ancestors) );
            $parent_slug = $parent_id->post_name;

            $page_template = $prefix.$post->post_name.$ext; //default template
            $parent_page_template = $prefix.$parent_slug.$ext; //ancestor template
            $parent_page_child_template = $prefix.$parent_slug.$appender.$ext; //children template

            $template = file_exists($templatePath.$page_template) ? $templatePath.$page_template : file_exists($templatePath.$parent_page_child_template) ? $templatePath.$parent_page_child_template : $templatePath.$parent_page_template;

            if (file_exists($template)) {
                load_template($template);
                exit;
            }
        } else {
            return true;
        }
    }
}
add_action('template_redirect','jsp_switch_page_template');

function jsp_switch_category_template() {
    if (is_category()) {
        $ancestors = get_ancestors(get_query_var('cat'),'category');
        $catname = get_query_var('category_name');
        if ($ancestors) {
            $templatePath = TEMPLATEPATH . '/';
            $prefix = 'category-'; //default page template prefix
            $ext = '.php';
            $appender = '_child';
            $parent_id = $ancestors[0];
            $parent_obj = get_term_by('id', $parent_id, 'category');
            $parent_slug = $parent_obj->slug;

            $category_template = $prefix.$catname.$ext; //default template
            $parent_category_template = $prefix.$parent_slug.$ext; //ancestor template
            $parent_category_child_template = $prefix.$parent_slug.$appender.$ext; //children template

            $template = file_exists($templatePath.$category_template) ? $templatePath.$category_template : file_exists($templatePath.$parent_category_child_template) ? $templatePath.$parent_category_child_template : $templatePath.$parent_category_template;

            if (file_exists($template)) {
                load_template($template);
                exit;
            }
        } else {
            return true;
        }
    }
}
add_action('template_redirect','jsp_switch_category_template');

function numeric_posts_nav() {

    if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="list-inline">' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li>%s</li>' . "\n", get_next_posts_link() );

    echo '</ul>' . "\n";

}

add_action('wp_ajax_nopriv_do_ajax', 'jsp_ajax_function');
add_action('wp_ajax_do_ajax', 'jsp_ajax_function');
function jsp_ajax_function(){
    $mode = 'json';

    switch($_REQUEST['fn']){
      case 'get_latest_posts':
        $output = ajax_get_latest_posts($_REQUEST['count']);
      break;
      case 'get_post_object':
        $output = ajax_get_post($_REQUEST['pid']);
      break;
      case 'get_post_meta':
        $output = ajax_get_post_meta($_REQUEST['pid']);
      break;
      default:
        $output = 'No function specified, check your jQuery.ajax() call';
      break;
    }

    if ($mode == 'raw') { //raw output
        echo $output;
    } else { // json output
        $output=json_encode($output);
        if(is_array($output)){
            print_r($output);
        }
        else{
            echo $output;
        }
    }

    die;

}

function ajax_get_latest_posts($count){
     $posts = get_posts('numberposts='.$count);
     return $posts;
}

function ajax_get_post($id) {
    $post = get_post( $id );
    return $post;
}

function ajax_get_post_meta($id) {
    $post = get_post_meta( $id );
    return $post;
}


include_once get_template_directory() . '/jsp_inc/mobile-detect/Mobile_Detect.php';
$detect = new Mobile_Detect;
$isMobile = $detect->isMobile();
$isTablet = $detect->isTablet();


function remove_images( $content ) {
   $postOutput = preg_replace('/<img[^>]+./','', $content);
   return $postOutput;
}

//remove_menu_page('tools.php');

require_once( $includes_path . '/_jsp_prefill_content.php' );
require_once( $includes_path . '/_jsp_sns_urls.php' );
require_once( $includes_path . '/_jsp_replace_wp_logo.php' );
require_once( $includes_path . '/_jsp_admin_revised_column.php' );
require_once( $includes_path . '/_jsp_author_contributions.php' );
require_once( $includes_path . '/_jsp_admin_custom_column.php' );
//require_once( $includes_path . '/_jsp_custom_roles.php' );

require_once( $includes_path . '/_jsp_dashboard.php' );

//song process
require_once( $includes_path . '/_jsp_songfilter.php');

//force login
require_once( $includes_path . '/_jsp_walled_garden.php' );

//require_once( $includes_path . '/_jsp_admin_styles.php' );

//require_once( $includes_path . '/_jsp_maintenance_mode.php' );
