<?php
/* custom post-types and taxonomies definitions */

//////////////////////////////// Songs Post Type ////////////////////////////////

function jsp_songs() {
    $labels = array(
        'name' => __( 'Songs' ),
        'singular_name' => __( 'Song' ),
        'add_new' => __( 'Add New' ),
        'add_new_item' => __( 'Create New Songsheet' ),
        'edit' => __( 'Edit' ),
        'edit_item' => __( 'Edit Songsheet' ),
        'new_item' => __( 'New Songsheet' ),
        'view' => __( 'View Songsheets' ),
        'view_item' => __( 'View Songsheet' ),
        'search_items' => __( 'Search Songsheets' ),
        'not_found' => __( 'No Songsheet found' ),
        'not_found_in_trash' => __( 'No Songsheet found in trash' ),
        'parent' => __( 'Parent Song' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'This is where you can create new Songsheets.' ),
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'capability_type' => array('song','songs'),
        'map_meta_cap' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-media-audio',
        'hierarchical' => false,
        '_builtin' => false, // It's a custom post type, not built in!
        'rewrite' => array(
            'slug'=> 'songs',
            'with_front'=> false,
            'feed'=> true,
            'pages'=> false
        ),
        'query_var' => true,
        'show_in_rest' => true,
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports' => array( 'title', 'editor', 'revisions', 'comments', 'author'),
    );
    register_post_type('songs', $args);

}

function jsp_songs_taxonomies() {
    register_taxonomy(
        'song-category', // taxonomy id
        array( // applies to which custom post types
            'songs',
        ),
        array(
            'hierarchical' => true,
            'label' => __( 'Song Categories' ),
            'singular_label' => __( 'Song Category' ),
            'show_admin_column' => true,
            'capabilities' => array(
                'manage_terms' => 'manage_song_category',
                'edit_terms' => 'manage_song_category',
                'delete_terms' => 'manage_song_category',
                'assign_terms' => 'edit_songs',
            ),
            'query_var' => true,
            'rewrite' => true,
        )
    );

    register_taxonomy(
        'song-tags', // taxonomy id
        array( // applies to which custom post types
            'songs'
        ),
        array(
            'hierarchical' => false,
            'label' => __( 'Song Tags' ),
            'singular_label' => __( 'Song Tag' ),
            'show_admin_column' => true,
            'capabilities' => array(
                'manage_terms' => 'manage_song_tags',
                'edit_terms' => 'manage_song_tags',
                'delete_terms' => 'manage_song_tags',
                'assign_terms' => 'edit_songs',
            ),
            'query_var' => true,
            'rewrite' => true,
        )
    );
}


function jsp_songs_rw_rules() {
    add_rewrite_tag('%rw_songs%','(songs)','post_type=');
    add_rewrite_tag('%song-cats%', '([^/]+)', 'post_type=songs&song-category=');
    //add_rewrite_tag('%song-tags%', '([^/]+)', 'post_type=songs&song-tags=');

    add_permastruct('songs_cats', 'songs/%song-cats%');
    //add_permastruct('songs_tags', 'songs/tags/%song-tags%');

    //add_rewrite_rule('^songs/tags/([^/]*)/?','index.php?post_type=songs&song-tags=$matches[1]','top');
    //add_rewrite_rule('^songs/([^/]*)/([^/]*)/?','index.php?post_type=songs&song-category=$matches[1]&name=$matches[2]','top');
    add_rewrite_rule('^songs/([^/]*)/?','index.php?post_type=songs&name=$matches[1]','top');
}


function jsp_songs_cat_link( $post_link, $id = 0 ) {  //define permalink for posts with categories

    $post = get_post($id);

    if ( is_wp_error($post) || 'songs' != $post->post_type || empty($post->post_name) )
        return $post_link;

    // $terms = get_the_terms($post->ID, 'song-category');

    // if( is_wp_error($terms) || !$terms ) {
    //     $tax = '';  // slug for uncategorized posts
    //     return home_url(user_trailingslashit( "songs/$post->post_name" ));
    // }
    // else {
    //     $tax_obj = array_pop($terms);
    //     $tax = $tax_obj->slug;

    //     return home_url(user_trailingslashit( "songs/$tax/$post->post_name" ));
    // }

    return home_url(user_trailingslashit( "songs/$post->post_name" ));
}

//add_action('init', 'jsp_songs_rw_rules');
add_action('init', 'jsp_songs');
add_action('init', 'jsp_songs_taxonomies');
//add_filter( 'post_type_link', 'jsp_songs_cat_link' , 10, 2 );

////////////////////////////////////////////////////////////////


//////////////////////////////// Lineups Post Type ////////////////////////////////

function jsp_lineups() {
    $labels = array(
        'name' => __( 'Lineups' ),
        'singular_name' => __( 'Lineup' ),
        'add_new' => __( 'Add New' ),
        'add_new_item' => __( 'Create New Lineup' ),
        'edit' => __( 'Edit' ),
        'edit_item' => __( 'Edit Lineup' ),
        'new_item' => __( 'New Lineup' ),
        'view' => __( 'View Lineups' ),
        'view_item' => __( 'View Lineup' ),
        'search_items' => __( 'Search Lineups' ),
        'not_found' => __( 'No Lineup found' ),
        'not_found_in_trash' => __( 'No Lineup found in trash' ),
        'parent' => __( 'Parent Lineup' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'This is where you can create new Songsheets.' ),
        'public' => true,
        'has_archive' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'capability_type' => array('lineup','lineups'),
        'map_meta_cap' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'menu_position' => 2,
        'menu_icon' => 'dashicons-playlist-audio',
        'hierarchical' => false,
        '_builtin' => false, // It's a custom post type, not built in!
        'rewrite' => array(
            'slug'=> 'lineups',
            'with_front'=> false,
            'feed'=> true,
            'pages'=> false
        ),
        'query_var' => true,
        'show_in_rest' => true,
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports' => array( 'title'),
    );
    register_post_type('lineups', $args);

}

function jsp_lineups_taxonomies() {
    register_taxonomy(
        'lineup-category', // taxonomy id
        array( // applies to which custom post types
            'lineups'
        ),
        array(
            'hierarchical' => true,
            'label' => __( 'Lineup Categories' ),
            'singular_label' => __( 'Lineup Category' ),
            'show_admin_column' => true,
            'capabilities' => array(
                'manage_terms' => 'manage_lineup_category',
                'edit_terms' => 'manage_lineup_category',
                'delete_terms' => 'manage_lineup_category',
                'assign_terms' => 'edit_lineups',
            ),
            'query_var' => true,
            'rewrite' => array( 'slug' => 'lineups', 'hierarchical' => true ),
        )
    );

    register_taxonomy(
        'lineup-tags', // taxonomy id
        array( // applies to which custom post types
            'lineups'
        ),
        array(
            'hierarchical' => false,
            'label' => __( 'Lineup Tags' ),
            'singular_label' => __( 'Lineups Tag' ),
            'show_admin_column' => true,
            'capabilities' => array(
                'manage_terms' => 'manage_lineup_tags',
                'edit_terms' => 'manage_lineup_tags',
                'delete_terms' => 'manage_lineup_tags',
                'assign_terms' => 'edit_lineups',
            ),
            'query_var' => true,
            'rewrite' => array( 'slug' => 'lineups/tags', 'hierarchical' => false),
        )
    );
}

function jsp_lineups_rw_rules() {
    add_rewrite_tag('%rw_lineups%','(lineups)','post_type=');
    //add_rewrite_tag('%lineup-cats%', '([^/]+)', 'post_type=lineups&lineup-category=');
    //add_rewrite_tag('%lineup-tags%', '([^/]+)', 'post_type=lineups&lineup-tags=');

    //add_permastruct('lineups_cats', 'lineups/%lineup-cats%');
    //add_permastruct('lineups_tags', 'lineups/tags/%lineup-tags%');

    //add_rewrite_rule('^lineups/tags/([^/]*)/?','index.php?post_type=lineups&lineup-tags=$matches[1]','top');
    //add_rewrite_rule('^lineups/([^/]*)/([^/]*)/?','index.php?post_type=lineups&lineup-category=$matches[1]&name=$matches[2]','top');
}


function jsp_lineups_cat_link( $post_link, $id = 0 ) {  //define permalink for posts with categories

    $post = get_post($id);

    if ( is_wp_error($post) || 'lineups' != $post->post_type || empty($post->post_name) )
        return $post_link;

    $terms = get_the_terms($post->ID, 'lineup-category');

    /*if( is_wp_error($terms) || !$terms ) {
        //$tax = '';  // slug for uncategorized posts
        return home_url(user_trailingslashit( "lineups/$post->post_name" ));
    }
    else {
        $tax_obj = array_pop($terms);
        $tax = $tax_obj->slug;

        return home_url(user_trailingslashit( "lineups/$tax/$post->post_name" ));
    }*/
    return home_url(user_trailingslashit( "lineups/$post->post_name" ));
}

//add_action('init', 'jsp_lineups_rw_rules');
add_action('init', 'jsp_lineups');
add_action('init', 'jsp_lineups_taxonomies');
//add_filter( 'post_type_link', 'jsp_lineups_cat_link' , 10, 2 );

////////////////////////////////////////////////////////////////

?>
