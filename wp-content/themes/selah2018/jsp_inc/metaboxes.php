<?php

$jsp_slideType = '';
function jsp_register_meta_boxes( $meta_boxes )
{
    global $jsp_slideType;
    $meta_boxes = array();

    $prefix = 'jsp_';

    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    }
    elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }
    else {
        $post_id = false;
    }

    // $meta_boxes[] = array(
    //     'title' => __( 'Featured Image', 'rwmb' ),
    //     'pages' => array( 'post', 'page' ),
    //     'context' => 'side',
    //     'priority' => 'low',
    //     'fields' => array(
    //         array(
    //             'name'             => __( '', 'rwmb' ),
    //             'id'               => "{$prefix}featuredImg",
    //             'type'             => 'image_advanced',
    //             'max_file_uploads' => 1,
    //         ),
    //     )
    // );

    // $meta_boxes[] = array(
    //     'title' => __( 'Thumbnail Image', 'rwmb' ),
    //     'pages' => array( 'page' ),
    //     'context' => 'side',
    //     'priority' => 'low',
    //     'only_on'    => array(
    //         'parent_slug'   => array('services')
    //     ),
    //     'fields' => array(
    //         array(
    //             'name'             => __( '', 'rwmb' ),
    //             'id'               => "{$prefix}thumbImg",
    //             'type'             => 'image_advanced',
    //             'max_file_uploads' => 1,
    //         ),
    //     )
    // );

    // $meta_boxes[] = array(
    //     'id'       => 'hero-image',
    //     'title'    => 'Hero Image',
    //     'pages'    => array( 'hero' ),
    //     'context'  => 'side',
    //     'priority' => 'default',
    //     'fields' => array(
    //         array(
    //             'name'             => __( '', 'rwmb' ),
    //             'id'               => "{$prefix}plupload_hero",
    //             'type'             => 'plupload_image',
    //             'max_file_uploads' => 1,
    //         ),
    //     ),
    //     /*'only_on'    => array(
    //         'id'       => array( 1, 2 ),
    //         'slug'  => array( 'news', 'blog' ),
    //         'template' => array( 'fullwidth.php', 'simple.php' ),
    //         'parent'   => array( 10 )
    //     ),*/
    // );

    // $cardargs = array(
    //     'posts_per_page'   => -1,
    //     'orderby'          => 'post_date',
    //     'order'            => 'DESC',
    //     'post_type'        => 'cards',
    //     'post_status'      => 'publish',
    //     'suppress_filters' => true
    // );
    // $cardsArr = get_posts( $cardargs );
    // $cards = '';
    // foreach ($cardsArr as $card) {
    //     $cards["$card->ID"] = $card->post_title;
    // }

    // $meta_boxes[] = array(
    //     'id'       => 'cards_list',
    //     'title'    => 'Cards',
    //     'pages'    => array( 'promotions', 'rewards' ),
    //     'context'  => 'side',
    //     'priority' => 'default',
    //     'fields' => array(
    //         array(
    //             'name' => __( '', 'rwmb' ),
    //             'id'   => "{$prefix}cards_list",
    //             'type' => 'checkbox_list',
    //             // Options of checkboxes, in format 'value' => 'Label'
    //             'options' => $cards
    //         ),
    //     ),
    // );

    $meta_boxes[] = array(
        'id'       => 'lineup-date',
        'title'    => 'Lineup Date',
        'pages'    => array( 'lineups' ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
				'name' => __( ' ', 'selah' ),
				'id'   => $prefix . 'lineup_date',
				'type' => 'date',

				// jQuery date picker options. See here http://jqueryui.com/demos/datepicker
				'js_options' => array(
					'appendText'      => __( ' (yyyy-mm-dd)', 'selah' ),
					'autoSize'        => true,
					'buttonText'      => __( 'Select Date', 'selah' ),
					'dateFormat'      => __( 'yy-mm-dd', 'selah' ),
					'numberOfMonths'  => 1,
					'showButtonPanel' => true,
				),
            ),
            array(
				'name'    => __( '', 'selah' ),
				'id'      => $prefix . 'lineup_cat',
                'type'    => 'taxonomy',
                'taxonomy' => 'lineup-category',
                'placeholder' => 'Select a category',
                'field_type' => 'select'
			),
        )
    );
    
    $meta_boxes[] = array(
        'id'       => 'lineup_list',
        'title'    => 'Songs',
        'pages'    => array( 'lineups'),
        'context'  => 'normal',
        'priority' => 'high',
        'fields' => array(
            // Group
            array(
                'name' => '', // Optional
                'id' => "{$prefix}lineup_song_item",
                'type' => 'group',
                'class' => 'row',
                'clone' => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name'        => esc_html__( 'Song', 'selah' ),
                        'id'          => "{$prefix}lineup_song",
                        'type'        => 'post',
                        'post_type'   => 'songs',
                        'field_type'  => 'select_advanced',
                        'multiple'    => false,
                        'placeholder' => esc_html__( 'Select a Song', 'selah' ),
                        'query_args'  => array(
                            'post_status'    => 'publish',
                            'posts_per_page' => - 1,
                        ),
                        'class' => ''
                    ),
                    array(
                        'name'        => esc_html__( 'Key', 'selah' ),
                        'id'          => "{$prefix}lineup_song_key",
                        'type'        => 'select',
                        'options'     => array(
                            'C' => 'C',
                            'Db' => 'Db',
                            'D' => 'D',
                            'Eb' => 'Eb',
                            'E' => 'E',
                            'F' => 'F',
                            'Gb' => 'Gb',
                            'G' => 'G',
                            'Ab' => 'Ab',
                            'A' => 'A',
                            'Bb' => 'Bb',
                            'B' => 'B',
                        ),
                        'placeholder' => esc_html__( 'Default Key', 'selah' ),
                    ),
                    array(
                        'name' => __( 'Notes', 'rwmb' ),
                        'desc' => __( '', 'rwmb' ),
                        'id'   => "{$prefix}lineup_song_notes",
                        'type' => 'textarea',
                        'rows' => 4,
                    ),
                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'id'       => 'song-information',
        'title'    => 'Song Information',
        'pages'    => array( 'songs' ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => __( 'Artist', 'rwmb' ),
                'desc' => __( '', 'rwmb' ),
                'id'   => "{$prefix}song_artist",
                'type' => 'text',
            ),
            array(
                'name' => __( 'Album', 'rwmb' ),
                'desc' => __( '', 'rwmb' ),
                'id'   => "{$prefix}song_album",
                'type' => 'text',
            ),
            array(
                'name' => __( 'Tempo', 'rwmb' ),
                'desc' => __( '', 'rwmb' ),
                'id'   => "{$prefix}song_bpm",
                'type' => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'id'       => 'song-resources',
        'title'    => 'Resources',
        'pages'    => array( 'songs', 'lineups' ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields' => array(
            // Group
            array(
                'name' => '', // Optional
                'id' => "{$prefix}resource_list",
                'type' => 'group',
                'clone' => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __( 'Description', 'rwmb' ),
                        'desc' => __( 'Video, Audio, Custom text', 'rwmb' ),
                        'id'   => "{$prefix}song_resource_description",
                        'type' => 'text',
                    ),
                    array(
                        'name' => __( 'URL', 'rwmb' ),
                        'desc' => __( '', 'rwmb' ),
                        'id'   => "{$prefix}song_resource_url",
                        'type' => 'text',
                    ),
                ),
            ),
        )
    );


	if ( class_exists( 'RW_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			if ( isset( $meta_box['only_on'] ) && ! rw_maybe_include( $meta_box['only_on'] ) ) {
				continue;
			}

			new RW_Meta_Box( $meta_box );
		}
	}
}

add_action( 'admin_init', 'jsp_register_meta_boxes' );


/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function rw_maybe_include( $conditions ) {
    global $jsp_slideType;

	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
		return false;
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return true;
	}

	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	}
	elseif ( isset( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}
	else {
		$post_id = false;
	}

	$post_id = (int) $post_id;
	$post    = get_post( $post_id );

	if ($post) {
		foreach ( $conditions as $cond => $v ) {
			if ( ! is_array( $v ) ) {
				$v = array( $v );
			}

			switch ( $cond ) {
				case 'id':
					if ( in_array( $post_id, $v ) ) {
						return true;
					}
				break;
				case 'parent':
					$post_parent = $post->post_parent;
					if ( in_array( $post_parent, $v ) ) {
						return true;
					}
				break;
				case 'slug':
					$post_slug = $post->post_name;
					if ( in_array( $post_slug, $v ) ) {
						return true;
					}
				break;
                case 'parent_slug':
                    $ancestors = $post->ancestors;
                    if ( $ancestors ) {
                        $parent_id = get_post( end($ancestors) );
                        $parent_slug = $parent_id->post_name;
                        if ( in_array( $parent_slug, $v ) ) {
                            return true;
                        }
                    }
                break;
                case 'slide':
                    if ( in_array( $jsp_slideType, $v ) ) {
                        return true;
                    }
                break;
                case 'category':
                    $categories = get_the_category( $post->ID );
                    $catslugs = array();
                    foreach ($categories as $category) {
                        array_push($catslugs, $category->slug);
                    }
                    if ( array_intersect( $catslugs, $v ) ) {
                        return true;
                    }
                break;
				case 'template':
					$template = get_post_meta( $post_id, '_wp_page_template', true );
					if ( in_array( $template, $v ) ) {
						return true;
					}
				break;
			}
		}
	}

	return false;
}

?>
