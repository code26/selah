<?php
function jsp_register_taxonomy_meta_boxes()
{
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( !class_exists( 'RW_Taxonomy_Meta' ) )
		return;

	$meta_sections = array();

	$prefix = 'jsp_ms_';

	$meta_sections[] = array(
		'title'      => 'Additional Fields',
		'taxonomies' => array('category','song-category','lineup-category'),
		'id'         => 'category-fields',                 // ID of each section, will be the option name

		'fields' => array(                             // List of meta fields
			array(
				'name' => 'Thumbnail',
				'id'   => "{$prefix}cat_thumb",
				'type' => 'image',
			),
		),
	);


	foreach ( $meta_sections as $meta_section )
	{
		new RW_Taxonomy_Meta( $meta_section );
	}
}
add_action( 'admin_init', 'jsp_register_taxonomy_meta_boxes' );

