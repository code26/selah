<?php /* Template Name: Simple */ ?>
<?php get_header(); ?>
<div id="page-container">
  <div class="page-container">
    <section class="page-section">
    <?php while ( have_posts() ) : the_post(); ?>

      <h2><?php the_title();?></h2>

      <?php
      the_content();
      ?>

    <?php endwhile; ?>
    </section>
  </div>
</div>
<?php get_footer();?>
