<?php if( current_user_can('edit_lineups') ) : ?>
<div class="item col-md-3 col-sm-4">
  <a href="<?php bloginfo( 'url' ); ?>/wp-admin/post-new.php?post_type=lineups">
    <div class="item-bounds item-new">
      + Create New Lineup
    </div>
  </a>
</div>
<?php endif;?>
