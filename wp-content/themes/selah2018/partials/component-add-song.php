<?php if( current_user_can('edit_songs') ) : ?>
<div class="item col-md-3 col-sm-4">
  <a href="<?php bloginfo( 'url' ); ?>/wp-admin/post-new.php?post_type=songs">
    <div class="item-bounds item-new">
      + Add New Song
    </div>
  </a>
</div>
<?php endif;?>
