<div class="notes-container visible-print">
  <div class="annotations">
    <div class="title">Annotations</div>
    <div class="icons">
      <div class="item"><span class="icon annotate-keyboard"></span><span class="icon-label">Keyboard</span></div>
      <div class="item"><span class="icon annotate-guitar"></span><span class="icon-label">Guitar</span></div>
      <div class="item"><span class="icon annotate-bass"></span><span class="icon-label">Bass</span></div>
      <div class="item"><span class="icon annotate-drum"></span><span class="icon-label">Drum</span></div>
      <div class="item"><span class="icon annotate-vocals"></span><span class="icon-label">Vocals</span></div>

      <div class="item"><span class="icon annotate-nokeyboard"></span><span class="icon-label">No Keyboard</span></div>
      <div class="item"><span class="icon annotate-noguitar"></span><span class="icon-label">No Guitar</span></div>
      <div class="item"><span class="icon annotate-nobass"></span><span class="icon-label">No Bass</span></div>
      <div class="item"><span class="icon annotate-nodrum"></span><span class="icon-label">No Drum</span></div>
      <div class="item"><span class="icon annotate-novocals"></span><span class="icon-label">No Vocals</span></div>
    </div>
    <div class="icons">
      <div class="item"><span class="icon annotate-fadein"></span><span class="icon-label">Fade In</span></div>
      <div class="item"><span class="icon annotate-fadeout"></span><span class="icon-label">Fade Out</span></div>
      <div class="item"><span class="icon annotate-breakin"></span><span class="icon-label">Break In</span></div>
      <div class="item"><span class="icon annotate-breakout"></span><span class="icon-label">Break Out</span></div>
      <div class="item"><span class="icon annotate-stacatto"></span><span class="icon-label">Stacatto Break</span></div>
    </div>
  </div>
</div>