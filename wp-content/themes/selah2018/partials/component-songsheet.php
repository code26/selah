      <?php
      $artist = rwmb_meta('jsp_song_artist');
      $featuredImg = rwmb_meta( 'jsp_featuredImg', 'type=image' );
      $featuredImgUrl = (end($featuredImg));
      if ($featuredImgUrl) {
      ?>
      <figure class="thumbnail-container featured-image">
        <img src="<?=$featuredImgUrl['full_url']?>" >
      </figure>
      <?php } ?>

      <header class="section-header">
        <h2 class="section-title"><?php the_title();?></h2>
      <?php if (!empty($artist)): ?>
        <div class="artist"><?=$artist?></div>
      <?php endif; ?>
      </header>

      <?php get_template_part('partials/component','toolbox')?>

      <pre class="sheet-content"><?php echo processSheet(get_the_content());?></pre>
    <?php
      $resources = rwmb_meta('jsp_resource_list');

      if (!empty($resources)):
    ?>
    <div class="sheet-section resources hidden-print">
      <h3 class="section-title">Resources</h3>
      <ul>
        <?php foreach($resources as $resource):?>
        <li><?=$resource["jsp_song_resource_description"]?>: <?=make_clickable($resource["jsp_song_resource_url"])?></li>
        <?php endforeach;?>
      </ul>
    </div>
    <?php endif;?>
