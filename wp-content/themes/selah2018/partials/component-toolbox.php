<div class="tools hidden-print">
  <div class="toolbox">
    <div class="form-group component-transpose">
      <select class="form-control transposer">
        <option value="C">C</option>
        <option value="C#">C#</option>
        <option value="D">D</option>
        <option value="D#">D#</option>
        <option value="E">E</option>
        <option value="F">F</option>
        <option value="F#">F#</option>
        <option value="G">G</option>
        <option value="G#">G#</option>
        <option value="A">A</option>
        <option value="A#">A#</option>
        <option value="B">B</option>
      </select>
    </div>
    <button class="btn mode-toggle" title="Show/Hide Chords">CHORDS</button>
    <div class="text-resize">
      <button class="btn btn-decrease btn-ico" title="Decrease Font Size"><span class="ico-text_fields"></span></button>
      <button class="btn btn-increase btn-ico" title="Increase Font Size"><span class="ico-format_size"></span></button>
    </div>
    <button class="btn btn-print btn-ico" title="Print"><span class="ico-print"></span></button>
    <button class="btn btn-print-with-notes btn-ico" title="Print with Notebox"><span class="ico-description"></span></button>
    <button class="btn btn-close btn-ico" title="Print with Notebox"><span class="ico-close"></span></button>
  </div>
  <div class="toolset">
    <?php if (current_user_can('edit_others_songs')) : ?>
    <a href="<?= get_edit_post_link();?>" class="btn btn-ico" title="Edit Song"><span class="ico-mode_edit"></span></a>
    <?php endif;?>
    <button class="btn btn-open-toolbox btn-ico" title="Open Tools"><span class="ico-more_horiz"></span></button>
  </div>

  <!-- <button class="btn btn-pdf"><span class="glyphicon glyphicon-file"></span></button> -->
</div>
