<?php
$keyword = sanitize_text_field($keyword);
$querystr = "
    SELECT DISTINCT wposts.*
    FROM $wpdb->posts wposts
    LEFT JOIN $wpdb->postmeta wpostmeta ON wposts.ID = wpostmeta.post_id
    LEFT JOIN $wpdb->term_relationships ON (wposts.ID = $wpdb->term_relationships.object_id)
    LEFT JOIN $wpdb->terms wpterms ON (wpterms.term_id = $wpdb->term_relationships.term_taxonomy_id)
    WHERE wposts.post_type = 'songs'
    AND wposts.post_status = 'publish'
    AND (
         wposts.post_title LIKE '%".$keyword."%'
      OR wposts.post_content LIKE '%".$keyword."%'
      OR wpostmeta.meta_value LIKE '%".$keyword."%'
      OR wpterms.name LIKE '".$keyword."'
        )
    ORDER BY wposts.post_title ASC
    ";

$querystr2 = "
    SELECT DISTINCT wposts.*
    FROM $wpdb->posts wposts
    WHERE wposts.post_type = 'songs'
    AND wposts.post_status = 'publish'
    ORDER BY wposts.post_title ASC
    ";

if (strlen($keyword) < 1) {
  $custom['query'] = $wpdb->get_results($querystr2, OBJECT);
} else if (strlen($keyword) > 3 )   {
  $custom['query'] = $wpdb->get_results($querystr, OBJECT);
} else {
  unset($custom['query']);
}

if ( $custom['query'] ):
?>
  <?php
    foreach ( $custom['query'] as $post) :
    setup_postdata( $post );
  ?>

  <?php include 'list-item-song.php'; ?>
  <?php endforeach; ?>

  <?php
  //include CTA button
  get_template_part('partials/component','add-song');
  ?>

<?php else: ?>
    <?php
    //include CTA button
    get_template_part('partials/component','add-song');
    ?>
<?php endif; wp_reset_postdata();?>
