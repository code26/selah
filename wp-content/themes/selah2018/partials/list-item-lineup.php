
<?php

    $ldate = rwmb_meta('jsp_lineup_date');
    $ldate = date_create($ldate);

    $cats = get_the_terms($post->ID, 'lineup-category');
    $cat = $cats[0]->name;

    // $lineupCats = get_the_terms($post->ID, 'lineup-category');
    // $lineupTags = get_the_terms($post->ID, 'lineup-tags');
    // $lineupCatIDs = $lineupCatNames = [];

    // if (!empty($lineupCats)) {
    //   foreach($lineupCats as $lineupCat) {
    //     array_push($lineupCatIDs,$lineupCat->term_id);
    //     array_push($lineupCatNames,$lineupCat->slug);
    //   }
    // }
    
  ?>
  <div class="item col-md-3 col-sm-4 <?=implode(' ',$lineupCatNames);?>" data-catids="<?=implode(',',$lineupCatIDs);?>">
    <a href="<?php the_permalink();?>">
      <div class="item-bounds">


        <h3 class="lineup-title"><?=date_format($ldate,"F j, Y")?><br><?=$cat?></h3>
        
        <div class="lineup-wl"><?php the_title();?></div>
      </div>
    </a>
  </div>
