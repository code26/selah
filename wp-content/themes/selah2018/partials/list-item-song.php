
<?php
    $artist = rwmb_meta('jsp_song_artist');
    $songCats = get_the_terms($post->ID, 'song-category');
    $songTags = get_the_terms($post->ID, 'song-tags');
    $songCatIDs = $songCatNames = [];

    if (!empty($songCats)) {
      foreach($songCats as $songCat) {
        array_push($songCatIDs,$songCat->term_id);
        array_push($songCatNames,$songCat->slug);
      }
    }
  ?>
  <div class="item col-md-3 col-sm-4 <?=implode(' ',$songCatNames);?>" data-catids="<?=implode(',',$songCatIDs);?>">
    <a href="<?php the_permalink();?>">
      <div class="item-bounds">
        <div class="song-artist"><?=$artist?></div>
        <h3 class="song-title"><?php the_title();?></h3>

          <ul class="song-tags">
            <?php if (!empty($songCats)): ?>
              <?php
                foreach($songCats as $songCat) {
              //print_r($songCat);
              ?>
                <li class="item-tag tag-<?=$songCat->slug;?>">
                  <?=$songCat->name;?>
                </li>
              <?php } ?>
            <?php endif;?>
            <?php if (!empty($songTags)): ?>
                <?php
                  foreach($songTags as $songTag) {
                //print_r($songCat);
                ?>
                  <li class="item-tag tag-<?=$songTag->slug;?>">
                    <?=$songTag->name;?>
                  </li>
                <?php } ?>
            <?php endif;?>
          </ul>

      </div>
    </a>
  </div>
