<div class="menu-container hidden-print">
  <div class="hamburger-container">
    <a href="#" class="menu-button" id="MenuButton">
      <span class="burger-icon"></span>
    </a>
  </div>
  <div class="primary-menu-container">
    <a href="<?=esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo.svg" alt="" class="logo" id="SiteLogo"></a>
    <div class="site-nav-container">
      <?php wp_nav_menu(array('container' => false, 'menu_class' => 'site-nav', 'theme_location' => 'primary', 'fallback_cb' => false));?>
      <a href="<?php echo get_dashboard_url(); ?>" class="sys-btn">DASHBOARD</a>
      <a href="<?php echo wp_logout_url(); ?>" class="sys-btn btn-logout">LOGOUT</a>
    </div>
  </div>
</div>
