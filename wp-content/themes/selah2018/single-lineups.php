<?php get_header(); ?>
<?php
$ldate = rwmb_meta('jsp_lineup_date');
$ldate = date_create($ldate);

$cats = get_the_terms($post->ID, 'lineup-category');
$cat = $cats[0]->name;
?>
<div id="page-container">
  <div class="page-container">
    <header class="lineup-header hidden-print">
      <div class="lineup-title">
        Lineup for <?=date_format($ldate,"F j, Y")?> <span class="emphasized"><?=$cat?></span>
        <?php if (current_user_can('edit_lineups')) : ?>
        <a href="<?= get_edit_post_link();?>" class="btn-link btn-reversed" title="Edit Lineup"><span class="ico-mode_edit"></span></a>
        <?php endif;?>
      </div>
    </header>

    <?php
      $lineup = rwmb_meta('jsp_lineup_song_item');
      if (!empty($lineup)):
        $songIDs = array();
        $songKeys = array();
        $songNotes = array();
        foreach($lineup as $song) {
          array_push($songIDs,$song['jsp_lineup_song']);
          $songKeys[$song['jsp_lineup_song']] = (object) [
            'key' =>  $song['jsp_lineup_song_key'],
          ];
          $songNotes[$song['jsp_lineup_song']] = (object) [
            'notes' =>  $song['jsp_lineup_song_notes'],
          ];
        }
    ?>

    <?php
      $args = array(
        'post_type' => array( 'songs' ),
        'post__in'  => $songIDs,
        'orderby' => 'post__in',
        'order' => 'ASC',
      );
      $songs = get_posts( $args );
      foreach ( $songs as $post ) :
        setup_postdata( $post );

        $songnotes = $songNotes[$post->ID]->notes;
    ?>
    <section class="page-section song-section" data-songkey="<?=$songKeys[$post->ID]->key?>" data-songID="<?=$post->ID?>">
    <?php get_template_part('partials/component','songsheet')?>

    <?php get_template_part('partials/component', 'annotations'); ?>

    <!-- <div class="visible-print">
      <span class="printables">
        <span><?="Printed: ".date("M j Y")?></span>
        <span class="orig-key"><?="Original Key: ".getKey(get_the_content())?></span>
      </span>
    </div> -->
    
    <?php 
    // SONG NOTES FOR THE LINEUP
    if (!empty($songnotes)) : ?>
      <div class="sheet-section lineup-note hidden-print">
        <h3 class="section-title">Lineup Notes:</h3>
        <div>
          <?=wpautop($songnotes)?>
      </div>
      </div>
    <?php endif; ?>

    </section>
    <?php endforeach; wp_reset_postdata();?>
    <?php endif; ?>

    <?php
      $resources = rwmb_meta('jsp_resource_list');

      if (!empty($resources)):
    ?>
    <div class="page-section resources lineup-resources hidden-print">
      <h3 class="section-title">Lineup Resources</h3>
      <ul>
        <?php foreach($resources as $resource):?>
        <li><?=$resource["jsp_song_resource_description"]?>: <?=make_clickable($resource["jsp_song_resource_url"])?></li>
        <?php endforeach;?>
      </ul>
    </div>
    <?php endif;?>
  </div>

</div>

<?php get_sidebar('site-menu'); ?>

<?php get_footer();?>
