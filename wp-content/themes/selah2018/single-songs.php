<?php get_header(); ?>
<section class="page-section song-section">
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part('partials/component','songsheet')?>
<?php endwhile; ?>

<?php get_template_part('partials/component', 'annotations'); ?>

</section>

<!-- <div class="visible-print">
  <span class="printables">
    <span><?="Printed: ".date("M j Y")?></span>
    <span class="orig-key"><?="Original Key: ".getKey(get_the_content())?></span>
  </span>
</div> -->



<?php get_sidebar('site-menu'); ?>

<?php get_footer();?>
